const electron = require('electron');
const url = require('url');
const path = require('path');

const {app, BrowserWindow, Menu, ipcMain} = electron;

let windowManager = {};

const menuBar = buildMenuBar();

// Listens for the app to be ready.
app.on('ready', function(){

    // Creates the main window.
    windowManager.mainWindow = new BrowserWindow({
        width:1024, 
        height:768,
        resizable: true,
        maximizable: true,
        show: false
    });

    windowManager.mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'main/main.html'),
        protocol: 'file:',
        slashes: true,
    }));

    windowManager.mainWindow.on('ready-to-show', function () {
        windowManager.mainWindow.show();
    });

    // Inserts menu.
    Menu.setApplicationMenu(menuBar);
    
    // Quits app when closed.
    windowManager.mainWindow.on('close', function() {
        app.quit();
    });
});

// Button functions.
ipcMain.on('companyConverterButton', openCompanyConverter);
ipcMain.on('gfxGeneratorButton', openGFXGenerator);

function openCompanyConverter() {

    windowManager.companyConverter = new BrowserWindow({
        width: 800,
        height: 600,
        resizable: false,
        maximizable: false,
        nodeIntegration: false,
        show: false
    });

    windowManager.companyConverter.loadURL(url.format({
        pathname: path.join(__dirname, 'companyConverter/companyConverter.html'),
        protocol: 'file:',
        slashes: true
    }));

    windowManager.companyConverter.on('close', function() {
        windowManager.companyConverter = null;
        windowManager.mainWindow.webContents.send('setButton', 'companyConverter', true);
        menuBar.getMenuItemById('companyConverter').enabled = true;
    });

    windowManager.companyConverter.once('ready-to-show', function() {

        windowManager.companyConverter.show();
        windowManager.companyConverter.focus();

        windowManager.mainWindow.webContents.send('setButton', 'companyConverter', false);

    });

    menuBar.getMenuItemById('companyConverter').enabled = false;

}

function openGFXGenerator() {

    windowManager.gfxGenerator = new BrowserWindow({
        width: 800,
        height: 600,
        resizable: false,
        maximizable: false,
        nodeIntegration: false,
        show: false
    });

    windowManager.gfxGenerator.loadURL(url.format({
        pathname: path.join(__dirname, 'gfxGenerator/gfxGenerator.html'),
        protocol: 'file:',
        slashes: true
    }));

    windowManager.gfxGenerator.on('close', function() {
        windowManager.companyConverter = null;
        windowManager.mainWindow.webContents.send('setButton', 'gfxGenerator', true);
        menuBar.getMenuItemById('gfxGenerator').enabled = true;
    });

    windowManager.gfxGenerator.once('ready-to-show', function() {

        windowManager.gfxGenerator.show();
        windowManager.gfxGenerator.focus();

        windowManager.mainWindow.webContents.send('setButton', 'gfxGenerator', false);

    });

    menuBar.getMenuItemById('gfxGenerator').enabled = false;

}

function buildMenuBar () {
    // Creates menu template
    let template = [
        {
            label: 'File',
            submenu: [
                {
                    label: 'Quit',
                    accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
                    click(){
                        app.quit();
                    }
                }
            ]
        },
        {
            label: 'Windows',
            submenu: [
                {
                    label: 'DH Company Converter',
                    accelerator: process.platform == 'darwin' ? 'Command+1' : 'Ctrl+1',
                    id: 'companyConverter',
                    click(){
                        openCompanyConverter();
                    }
                },
                {
                    label: 'GFX File Generator',
                    accelerator: process.platform == 'darwin' ? 'Command+2' : 'Ctrl+2',
                    id: 'gfxGenerator',
                    click(){
                        openGFXGenerator();
                    }
                }
            ]
        },
    ];

    // If Mac add empty object to menu.
    if(process.platform == 'darwin'){
        template.unshift({});
    }

    // Add developer tools if not in production.
    if(process.env.NODE_ENV != 'production'){
        template.push({
            label: 'Developer Tools',
            submenu:[
                {
                    label: 'Toogle Developer Tools',
                    accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
                    click(){
                        windowManager.mainWindow.toggleDevTools();
                    }
                },
                {
                    role: 'reload'
                }
            ]
        });
    }

    return Menu.buildFromTemplate(template);

}