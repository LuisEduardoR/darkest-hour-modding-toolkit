const electron = require('electron');
const {remote} = electron;
const {BrowserWindow, Menu, dialog} = remote;

const url = require('url');
const path = require('path');
const fs = require('fs');

const currentWindow = remote.getCurrentWindow();
let conversionWindow;

const menuBar = buildMenuBar();
currentWindow.setMenu(menuBar);

let inputDir = getDefaultDir();
let outputDir = getDefaultDir();

let inputFiles = getCsvFileNamesList(inputDir);

// Initialy sets the interface values.
updateInterface();

// Add the event listeners to the buttons.
document.getElementById('inputButton').addEventListener('click', selectInputDir);
document.getElementById('outputButton').addEventListener('click', selectOutputDir);
document.getElementById('reloadButton').addEventListener('click', function() {
    inputFiles = getCsvFileNamesList(inputDir);
    updateInputFileListUI();
});
document.getElementById('convertButton').addEventListener('click', function() {
    if(inputFiles.length > 0)
    {
        disableInterfaceButtons()
        createConversionWindow();
    }
});

// Receive the actions from the main code.// Selects the input directory.
function selectInputDir() {

    let dir = dialog.showOpenDialog(currentWindow, {
        properties: ['openDirectory']
    });

    if(dir != undefined) {
        inputDir = dir[0];
    }

    inputFiles = getCsvFileNamesList(inputDir);
    updateInterface();

}

// Selects the output directory.
function selectOutputDir() {

    let dir = dialog.showOpenDialog(currentWindow, {
        properties: ['openDirectory']
    });

    if(dir != undefined) {
        outputDir = dir[0];
    }

    updateInterface();

}

function updateInterface() {

    // Updates the input directory path text.
    document.getElementById('inputText').innerHTML = removeScriptInjection(inputDir);
    document.getElementById('outputText').innerHTML = removeScriptInjection(outputDir);

    const lu = document.getElementById('inputFilesList');

    while(lu.firstChild) {
        lu.removeChild(lu.firstChild);
    }

    if(inputFiles.length > 0) {

        document.getElementById("convertButton").disabled = false;

        for(let i = 0; i < inputFiles.length; i++) {
        
            const li = document.createElement('li');
            const itemText = document.createTextNode(removeScriptInjection(inputFiles[i]));
            li.appendChild(itemText);
            lu.appendChild(li);
        
        }

    } else {

        document.getElementById("convertButton").disabled = true;

        const li = document.createElement('li');
        const itemText = document.createTextNode('No .csv files found!');
        li.appendChild(itemText);
        lu.appendChild(li);

    }

}

function getDefaultDir() {

    let dir = __dirname;

    if(process.platform == 'win32') {

        dir = dir.split('\\');

        let defaultDir = '';

        for(let i = 0; i < dir.length; i++) {

            if(dir[i] == 'resources' && dir[i+1] == 'app.asar') {
                break;
            }

            if(i > 0)
            {
                defaultDir += '\\';
            }

            defaultDir += dir[i];

        }

        return defaultDir;

    } else if(process.platform == 'linux') {

        dir = dir.split('/');

        let defaultDir = '';

        for(let i = 0; i < dir.length; i++) {

            if(dir[i] == 'resources' && dir[i+1] == 'app.asar') {
                break;
            }

            if(i > 0)
            {
                defaultDir += '/';
            }

            defaultDir += dir[i];

        }

        return defaultDir;

    }

    return dir;

}

function getCsvFileNamesList (dir) {

    let fileNames = [];

    let files = fs.readdirSync(dir);

    for(f of files) {

        if(f.substring(f.length - 4, f.length) == '.csv') {
            fileNames.push(f);
        }

    }

    return fileNames;

}

function disableInterfaceButtons() {

    document.getElementById("inputButton").disabled = true;
    document.getElementById("outputButton").disabled = true;
    document.getElementById("convertButton").disabled = true;

}

function enableInterfaceButtons(enableConvertButton) {

    document.getElementById("inputButton").disabled = false;
    document.getElementById("outputButton").disabled = false;
    document.getElementById("convertButton").disabled = enableConvertButton;

}

function convertCsv() {

    let consoleText = 'Starting conversion...<br>';
    conversionWindow.webContents.send('updateConsole', consoleText);

    let successes = 0;

    for(csv of inputFiles)
    {

        consoleText += '- Converting ' + csv + '...<br>';
        conversionWindow.webContents.send('updateConsole', consoleText);  

        const content = fs.readFileSync(path.join(inputDir, csv), 'latin1');
        
        consoleText += '-- Reading .csv file...<br>';
        conversionWindow.webContents.send('updateConsole', consoleText);  

        const rows = content.split('\n');
        const header = rows[0].split(';');
    
        if(rows.length < 2 || header[0].length != 3) {
            consoleText += '<span style = "color: rgb(234, 165, 70)">-- Badly formated .csv! Skipping...<br></span>';
            conversionWindow.webContents.send('updateConsole', consoleText);
            continue;
        }

        consoleText += '-- Row 1 (Header): read sucessfully!<br>';
        conversionWindow.webContents.send('updateConsole', consoleText);

        const nation = rows[0].substring(0,3);
    
        let ret = rowsToCompany(rows, nation, consoleText);

        companies = ret.companies;
        consoleText = ret.console;

        consoleText += '-- ' + companies.length + ' of ' + (rows.length - 1) + ' rows converted!<br>';
        conversionWindow.webContents.send('updateConsole', consoleText);

        let output = 'ideas = {\n#################################################\n### Research Teams\n#################################################\n    Research_Team = {\n';

        for(let i = 0; i < companies.length; i++) {
            output += companyToOutputText(companies[i], nation);
        }

        output += '    }\n}';

        const fileName = 'DH_ResearchTeams_' + nation + '.txt';

        consoleText += '-- Writing .txt file...<br>';
        conversionWindow.webContents.send('updateConsole', consoleText); 

        fs.writeFileSync(path.join(outputDir, fileName), output, 'utf-8');
        successes++;

        consoleText += '-- Done!<br>';
        consoleText += '-- Generating localisation files...<br>';
        conversionWindow.webContents.send('updateConsole', consoleText);     
        
        ret = generateLocalisationFile(companies, nation, consoleText);

        consoleText = ret.console;

        consoleText += '-- Done!<br>';
        conversionWindow.webContents.send('updateConsole', consoleText);   

    }

    if(successes > 0)
    {
        consoleText += '<span style = "color: rgb(34,139,34)">Finished! ' + successes + ' of ' + inputFiles.length + ' .csv files converted succesfully!<br></span>';
    } else {
        consoleText += '<span style = "color: rgb(234, 165, 70)">Finished! ' + successes + ' of ' + inputFiles.length + ' .csv files converted succesfully!<br></span>';
    }
    conversionWindow.webContents.send('updateConsole', consoleText);

    conversionWindow.webContents.send('finishedConversion');
    enableInterfaceButtons(inputFiles > 0 ? true : false);
}

// Convert csv text to companies.
function rowsToCompany(rows, nation, consoleText) {

    let companies = [];

    for(let i = 1; i < rows.length; i++)
    {
        
        const cells = rows[i].split(';');

        if(cells.length < 6) {
            consoleText += '<span style = "color: rgb(234, 165, 70)">-- Row ' + (i + 1) + ': Bad formating! Ignoring...<br></span>';
            continue;
        }

        // Reads the cells and gets the company data.
        let company = {};

        company.name = cells[1];
        company.formatedName = formatCompanyName(company.name, nation);
        company.formatedName = checkDuplicateBonus(company.formatedName, companies);

        if(cells[3] == '' || cells[4] == '' || cells[5] == '') {
            consoleText += '<span style = "color: rgb(234, 165, 70)">-- Row ' + (i + 1) + ': Invalid data! Ignoring...<br></span>';
            conversionWindow.webContents.send('updateConsole', consoleText);
            continue;
        }

        company.skill = Number(cells[3]);
        company.startYear = Number(cells[4]);
        company.endYear = Number(cells[5]);
        
        if(Number.isNaN(company.skill) || Number.isNaN(company.startYear) || Number.isNaN(company.endYear)) {
            consoleText += '<span style = "color: rgb(234, 165, 70)">-- Row ' + (i + 1) + ': Invalid data! Ignoring...<br></span>';
            conversionWindow.webContents.send('updateConsole', consoleText);
            continue;
        }

        company.specialities = [];
        for(let i = 6; cells[i] != '' && i < 32; i++) {

            company.specialities.push(cells[i]);

        }

        companies.push(company);

        consoleText += '-- Row ' + (i + 1) + ' (' + company.name + '): converted succesfully!<br>';
        conversionWindow.webContents.send('updateConsole', consoleText);

    }

    return {companies:companies,console:consoleText};
}

function formatCompanyName (name, nation) {

    // Remove accentiated characters.
    name = name.normalize('NFD').replace(/[\u0300-\u036f]/g, "");

    // Swaps separator characters for '_'.
    name = name.replace(/-/g, '_');
    name = name.replace(/&/g, '_');
    name = name.replace(/ /g, '_');
    // Removes successive '_' that may occur. Done two times because, for example, 'a & b' would result '___' wich would be turned into '__' and then '_'.
    name = name.replace('__', '_');
    name = name.replace('__', '_');

    // Removes invalid characters that still remain.
    name = name.replace(/[^a-z/A-Z/_]/g, "");

    // Adds the natio prefix.
    name = nation + '_' + name;

    return name;

}

function checkDuplicateBonus(formatedName, companies) {

    for(company of companies) {

        // If there is a duplicate, adds a number at the end.
        if(formatedName == company.formatedName) {

            // Splits the string.
            formatedName = formatedName.split('_');

            // Checks if the last part of the splited string isn't already a number. 
            if(isNaN(formatedName[formatedName.length-1])) {

                // If it's not rebuilds the string and adds _2 at the end.
                let name = '';

                for(let i = 0; i < formatedName.length; i++) {
                    name += formatedName[i] + '_';
                }

                name += '2';

                // Checks if the new string isn't also a duplicate.
                return checkDuplicateBonus(name, companies);

            } else {

                // If it's rebuilds the string and adds the next number at the end.
                let name = '';
                const num = Number(formatedName[formatedName.length-1]) + 1;

                for(let i = 0; i < formatedName.length-1; i++) {
                    name = formatedName[i] + '_';
                }

                name += num;

                // Checks if the new string isn't also a duplicate.
                return checkDuplicateBonus(name, companies);

            }
        }
    }

    return formatedName;

}

function companyToOutputText(company, nation) {

    let text = '        # ' + company.name + ' (Skill : ' + company.skill +')\n';
    text += '        ' + company.formatedName + ' = {\n';
    text += '            picture = ' + company.formatedName + '\n';
    text += '            allowed = { original_tag = ' + nation + ' }\n';
    text += '            visible = {\n                date > ' + company.startYear + '.1.1\n                date < '  + company.endYear + '.1.1\n            }\n';
    text += '            research_bonus = {\n';

    for(let i = 0; i < company.specialities.length; i++) {
        text += '                ' + company.specialities[i] + ' = ' + (company.skill/100.0).toFixed(2) + '\n';
    }

    text += '            }\n            traits = {  }\n        }\n';

    return text;
}

function generateLocalisationFile(companies, nation, consoleText) {

    let output = '\ufeff';
    output += 'l_english:\n';

    for(company of companies) {

        output += ' ' + company.formatedName + ': "' + company.name + '"\n'

    }

    const fileName = 'dh_research_teams_' + nation + '_l_english.yml';

    consoleText += '-- Writing .yml file...<br>';
    conversionWindow.webContents.send('updateConsole', consoleText); 

    fs.writeFileSync(path.join(outputDir, fileName), output, 'utf-8');

    return {console:consoleText}

}

function createConversionWindow() {
    // Creates the main window.
    conversionWindow = new BrowserWindow(
        {
            width:600, 
            height:240,
            resizable: false,
            minimizable: false,
            parent:currentWindow,
            show: false
        });

    conversionWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'conversionWindow.html'),
        protocol: 'file:',
        slashes: true,
    }));

    conversionWindow.setMenu(Menu.buildFromTemplate([]));
    conversionWindow.setMenuBarVisibility(false);

    conversionWindow.once('ready-to-show', function() {

        conversionWindow.show();
        conversionWindow.focus();

        convertCsv();

    });

}

// Removes characters that may be used to exploit HTML code.
function removeScriptInjection(text) {

    return text.replace(/<script>/g,'').replace(/<\/script>/g,'');

}

function buildMenuBar () {
    // Creates menu template
    let template = [
        {
            label: 'File',
            submenu: [
                {
                    label: 'Browse for input directory',
                    click(){
                        selectInputDir();
                    }
                },
                {
                    label: 'Browse for output directory',
                    click(){
                        selectOutputDir();
                    }
                },
                {
                    label: 'Reload .csv file list',
                    click(){
                        inputFiles = getCsvFileNamesList(inputDir);
                        updateInputFileListUI();
                    }
                },
                {
                    label: 'Close',
                    accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
                    click(){
                        currentWindow.close();
                    }
                }
            ]
        }
    ];

    // If Mac add empty object to menu.
    if(process.platform == 'darwin'){
        template.unshift({});
    }

    // Add developer tools if not in production.
    if(process.env.NODE_ENV != 'production'){
        template.push({
            label: 'Developer Tools',
            submenu:[
                {
                    label: 'Toogle Developer Tools',
                    accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
                    click(){
                        currentWindow.toggleDevTools();
                    }
                },
                {
                    role: 'reload'
                }
            ]
        });
    }

    return Menu.buildFromTemplate(template);

}