const electron = require('electron');
const {ipcRenderer} = electron;


document.getElementById('companyConverter').addEventListener('click', function()
{
    ipcRenderer.send('companyConverterButton');
});

document.getElementById('gfxGenerator').addEventListener('click', function()
{
    ipcRenderer.send('gfxGeneratorButton');
});

ipcRenderer.on('setButton', function(e, name, state){

    document.getElementById(name).disabled = !state;

});