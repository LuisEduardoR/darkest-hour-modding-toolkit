const electron = require('electron');
const {remote} = electron;
const {BrowserWindow, Menu, dialog} = remote;

const url = require('url');
const path = require('path');
const fs = require('fs');

const currentWindow = remote.getCurrentWindow();
let generationWindow;

const menuBar = buildMenuBar();
currentWindow.setMenu(menuBar);

let inputDir = getDefaultDir();
let outputDir = getDefaultDir();
let outputFileName = getOutputFileName();

let inputFiles = getImgFileNamesList(inputDir);

// Initialy sets the interface values.
updateInterface();

// Add the event listeners to the buttons.
document.getElementById('inputButton').addEventListener('click', selectInputDir);
document.getElementById('outputButton').addEventListener('click', selectOutputDir);
document.getElementById('reloadButton').addEventListener('click', function() {
    inputFiles = getImgFileNamesList(inputDir);
    updateInputFileListUI();
});
document.getElementById('convertButton').addEventListener('click', function() {
    if(inputFiles.length > 0)
    {
        disableInterfaceButtons()
        createGenerationWindow();
    }
});

// Receive the actions from the main code.// Selects the input directory.
function selectInputDir() {

    let dir = dialog.showOpenDialog(currentWindow, {
        properties: ['openDirectory']
    });

    if(dir != undefined) {
        inputDir = dir[0];
    }

    outputFileName = getOutputFileName();

    inputFiles = getImgFileNamesList(inputDir);
    updateInterface();

}

// Selects the output directory.
function selectOutputDir() {

    let dir = dialog.showOpenDialog(currentWindow, {
        properties: ['openDirectory']
    });

    if(dir != undefined) {
        outputDir = dir[0];
    }

    updateInterface();

}

function updateInterface() {

    // Updates the input directory path text.
    document.getElementById('inputText').innerHTML = removeScriptInjection(inputDir);

    if(process.platform == 'win32')
    {
        document.getElementById('outputText').innerHTML = removeScriptInjection(outputDir + '\\' + outputFileName);
    }
    else // if (process.platform = 'linux') *need to test for mac but probably will be linux like.
    {
        document.getElementById('outputText').innerHTML = removeScriptInjection(outputDir + '/' + outputFileName);
    }
    

    const lu = document.getElementById('inputFilesList');

    while(lu.firstChild) {
        lu.removeChild(lu.firstChild);
    }

    if(inputFiles.length > 0) {

        document.getElementById("convertButton").disabled = false;

        for(let i = 0; i < inputFiles.length; i++) {
        
            const li = document.createElement('li');
            const itemText = document.createTextNode(removeScriptInjection(inputFiles[i]));
            li.appendChild(itemText);
            lu.appendChild(li);
        
        }

    } else {

        document.getElementById("convertButton").disabled = true;

        const li = document.createElement('li');
        const itemText = document.createTextNode('No .dds/.tga/.png files found!');
        li.appendChild(itemText);
        lu.appendChild(li);

    }

}

function getDefaultDir() {

    let dir = __dirname;

    if(process.platform == 'win32') {

        dir = dir.split('\\');

        let defaultDir = '';

        for(let i = 0; i < dir.length; i++) {

            if(dir[i] == 'resources' && dir[i+1] == 'app.asar') {
                break;
            }

            if(i > 0)
            {
                defaultDir += '\\';
            }

            defaultDir += dir[i];

        }

        return defaultDir;

    } else if(process.platform == 'linux') {

        dir = dir.split('/');

        let defaultDir = '';

        for(let i = 0; i < dir.length; i++) {

            if(dir[i] == 'resources' && dir[i+1] == 'app.asar') {
                break;
            }

            if(i > 0)
            {
                defaultDir += '/';
            }

            defaultDir += dir[i];

        }

        return defaultDir;

    }

    return dir;

}

function getImgFileNamesList (dir) {

    let fileNames = [];

    let files = fs.readdirSync(dir);

    for(f of files) {

        if(f.substring(f.length - 4, f.length) == '.dds' || f.substring(f.length - 4, f.length) == '.tga' || f.substring(f.length - 4, f.length) == '.png') {
            fileNames.push(f);
        }

    }

    return fileNames;

}

function getOutputFileName () {

    // Gets the fileName.
    let fileName; 
    if(process.platform == 'win32') {
        fileName = inputDir.split('\\');
    } else // if (process.platform = 'linux') *need to test for mac but probably will be linux like.
    {
        fileName = inputDir.split('/');
    }

    fileName = fileName[fileName.length - 1];
    fileName = formatGFXName(fileName);
    fileName += '.gfx'
    
    return 'DH_' + fileName;

}

function disableInterfaceButtons() {

    document.getElementById("inputButton").disabled = true;
    document.getElementById("outputButton").disabled = true;
    document.getElementById("convertButton").disabled = true;

}

function enableInterfaceButtons(enableConvertButton) {

    document.getElementById("inputButton").disabled = false;
    document.getElementById("outputButton").disabled = false;
    document.getElementById("convertButton").disabled = enableConvertButton;

}


function createGenerationWindow() {
    // Creates the main window.
    generationWindow = new BrowserWindow(
        {
            width:600, 
            height:240,
            resizable: false,
            minimizable: false,
            parent:currentWindow,
            show: false
        });

    generationWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'generationWindow.html'),
        protocol: 'file:',
        slashes: true,
    }));

    generationWindow.setMenu(Menu.buildFromTemplate([]));
    generationWindow.setMenuBarVisibility(false);

    generationWindow.once('ready-to-show', function() {

        generationWindow.show();
        generationWindow.focus();

        generateGFX();

    });

}

function generateGFX() {
    
    let consoleText = 'Generating .gfx...<br>';
    generationWindow.webContents.send('updateConsole', consoleText);  

    let pathName;
    if(process.platform == 'win32') {
        pathName = inputDir.split('\\');
    } else // if (process.platform = 'linux') *need to test for mac but probably will be linux like.
    {
        pathName = inputDir.split('/');
    }

    let prefix = 'GFX_';
    let relativePath = '';
    let correctRelativePath = false;
    for(let i = 0; i < pathName.length; i++) {

        if(pathName[i] == 'gfx') {
            correctRelativePath = true;
            relativePath += pathName[i] + '/';
            continue;
        }

        if(correctRelativePath) {
            relativePath += pathName[i] + '/';
            prefix += pathName[i] + '_';
        }      

    }

    let output = 'spriteTypes={\n';

    for(file of inputFiles) {

        id = file;
        id = id.split('.');
        id = id[0];

        output += '    spriteType={\n';
        output += '        name = "' + formatGFXName(prefix) + formatGFXName(id) + '"\n';
        output += '        texturefile = "' + relativePath + file + '"\n';
        output += '    }\n';

    }

    output += '}\n';

    consoleText += '- Writing .gfx file...<br>';
    generationWindow.webContents.send('updateConsole', consoleText); 

    fs.writeFileSync(path.join(outputDir, outputFileName), output, 'utf-8');

    consoleText += '<span style = "color: rgb(34,139,34)">Finished! ' + inputFiles.length + ' entries added to .gfx file!<br></span>';
    generationWindow.webContents.send('updateConsole', consoleText);

    generationWindow.webContents.send('finishedConversion');
    enableInterfaceButtons(inputFiles > 0 ? true : false);

}

function formatGFXName (name) {

    // Remove accentiated characters.
    name = name.normalize('NFD').replace(/[\u0300-\u036f]/g, "");

    // Swaps separator characters for '_'.
    name = name.replace(/-/g, '_');
    name = name.replace(/&/g, '_');
    name = name.replace(/ /g, '_');
    // Removes successive '_' that may occur. Done two times because, for example, 'a & b' would result '___' wich would be turned into '__' and then '_'.
    name = name.replace('__', '_');
    name = name.replace('__', '_');

    // Removes invalid characters that still remain.
    name = name.replace(/[^a-z/A-Z/0-9/_]/g, "");

    return name;

}

// Removes characters that may be used to exploit HTML code.
function removeScriptInjection(text) {

    return text.replace(/<script>/g,'').replace(/<\/script>/g,'');

}

function buildMenuBar () {
    // Creates menu template
    let template = [
        {
            label: 'File',
            submenu: [
                {
                    label: 'Browse for input directory',
                    click(){
                        selectInputDir();
                    }
                },
                {
                    label: 'Browse for output directory',
                    click(){
                        selectOutputDir();
                    }
                },
                {
                    label: 'Reload .dds/.tga/.png file list',
                    click(){
                        inputFiles = getImgFileNamesList(inputDir);
                        updateInputFileListUI();
                    }
                },
                {
                    label: 'Close',
                    accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
                    click(){
                        currentWindow.close();
                    }
                }
            ]
        }
    ];

    // If Mac add empty object to menu.
    if(process.platform == 'darwin'){
        template.unshift({});
    }

    // Add developer tools if not in production.
    if(process.env.NODE_ENV != 'production'){
        template.push({
            label: 'Developer Tools',
            submenu:[
                {
                    label: 'Toogle Developer Tools',
                    accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
                    click(){
                        currentWindow.toggleDevTools();
                    }
                },
                {
                    role: 'reload'
                }
            ]
        });
    }

    return Menu.buildFromTemplate(template);

}