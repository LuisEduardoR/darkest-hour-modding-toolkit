const electron = require('electron');
const {ipcRenderer} = electron;

// Removes characters that may be used to exploit HTML code.
function removeScriptInjection(text) {

    return text.replace(/<script>/g,'').replace(/<\/script>/g,'');

}

// Receive the actions from the main code.

ipcRenderer.on('updateConsole', function(e, text){

    document.getElementById('console').innerHTML = removeScriptInjection(text);

});

ipcRenderer.on('finishedConversion', function(e){

    document.querySelector('h1').innerHTML = 'Done!';

});